output "id" {
  description = "Ressource ID"
  value       = aws_iam_instance_profile.this.id
}

output "name" {
  description = "Ressource ID"
  value       = aws_iam_instance_profile.this.name
}